export const environment = {
  production: true,
  urlapi: 'https://tgsserver.com/El_Manguito/laravel_manguito/',
  urlproducts: 'https://tgsserver.com/El_Manguito/rest_manguito/public/products/',
  urlcategorias: 'https://tgsserver.com/El_Manguito/rest_manguito/public/categorias/',

};
