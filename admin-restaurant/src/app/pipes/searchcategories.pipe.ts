import { Category } from './../models/category';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchcategories'
})
export class SearchcategoriesPipe implements PipeTransform {

  transform(arreglo: Category[], texto: string = ''): any {
    if(texto === '') {
      return arreglo;
    }

    if(!arreglo) {
      return arreglo
    }

    return arreglo.filter(
      item => JSON.stringify(item).toLocaleLowerCase().includes(texto)
    );
  }

}
