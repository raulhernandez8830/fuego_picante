import { Product } from 'src/app/models/product';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchproducts'
})
export class SearchproductsPipe implements PipeTransform {

  transform(arreglo: Product[], texto: string = ''): any {
    if(texto === '') {
      return arreglo;
    }

    if(!arreglo) {
      return arreglo
    }

    return arreglo.filter(
      item => JSON.stringify(item).toLocaleLowerCase().includes(texto)
    );
  }

}
