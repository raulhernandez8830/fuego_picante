import { TipoMenu } from './../models/tipo_menu';
import { environment } from 'src/environments/environment';
import { GlobalService } from './global.service';
import { Category } from './../models/category';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserStore } from '../models/user_store';


@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url = environment.urlapi;

  constructor(private http: HttpClient,
    private globalservice: GlobalService) { }


  // listar categorias
  getCategorias(userstore: UserStore): Observable<Category[]> {

    return this.http.post(this.url + 'getcategorias', userstore).pipe(
      map(data => data as Category[])
    );
  }

  saveCategoria(categoria: Category, imagen): Observable<Category> {
    var myFormData = new FormData();
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    myFormData.append('nombre',categoria.nombre);
    myFormData.append('descripcion',categoria.descripcion);
    myFormData.append('imagen',imagen);
    myFormData.append('user_id',categoria.user_id.toString());
    myFormData.append('store_id',categoria.store_id.toString());



    return this.http.post(this.url + 'saveinvcategoria', myFormData, {
      headers: headers
    }).pipe(
      map(data => data as Category)
    );
  }

  deleteCategory(c: Category): Observable<Category> {
    return this.http.post(this.url + 'deletecategoria', c).pipe(
      map( data => data as Category)
    );
  }

  updateCategory(categoria: Category, imagen): Observable<Category> {

    var myFormData = new FormData();
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    myFormData.append('id',categoria.id.toString());
    myFormData.append('nombre',categoria.nombre);
    myFormData.append('descripcion',categoria.descripcion);
    myFormData.append('imagen',imagen);
    myFormData.append('user_id',categoria.user_id.toString());
    myFormData.append('store_id',categoria.store_id.toString());

    return this.http.post(this.url + 'editcategoria', myFormData, {
      headers: headers
    } ).pipe(
      map(data => data as Category)
    );
  }

  // crear nuevos tipos de menus
  saveTipoMenu(tipomenu: TipoMenu): Observable<TipoMenu> {
    return this.http.post(this.url + 'savetipomenu',tipomenu).pipe(
      map( data => data as TipoMenu)
    );
  }


}
