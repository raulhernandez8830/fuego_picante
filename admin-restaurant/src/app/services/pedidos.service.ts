import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {

  url = environment.urlapi;

  constructor(private http: HttpClient) { }

  // obtener pedidos recientes
  getPedidos(store):Observable<any[]> {
    return this.http.get(this.url + 'getpedidosforadmin?store_id='+ store).pipe(
      map(data => data as any[])
    );
  }

  getDetailPedido(pedido): Observable<any[]> {
    return this.http.get(this.url + 'getdetallepedidos?pedido_id=' + pedido.id).pipe(
      map(data => data as any[])
    );
  }

}
