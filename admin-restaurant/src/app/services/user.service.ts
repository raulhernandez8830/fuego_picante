import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = environment.urlapi;

  constructor(private globalservice: GlobalService, private http: HttpClient) { }

  // metodo para validar credenciales del usuario
  validarCredenciales(usuario: User): Observable<User> {
    return this.http.post<User>(this.url + 'validarcredenciales', usuario).pipe(
      map(data => data as User)
    );

  }
}
