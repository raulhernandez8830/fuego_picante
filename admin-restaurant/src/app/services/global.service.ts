import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  url = 'https://tgsserver.com/El_Manguito/laravel_manguito/';
  // url = 'http://localhost:8000/';
  constructor() {

  }

  public getBackendUrl() {
    return this.url;
  }
}
