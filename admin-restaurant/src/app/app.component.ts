import { Component, isDevMode } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { UserStore } from './models/user_store';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  userlogged: UserStore = new UserStore();

  constructor(private router: Router, private menu: MenuController) {}

  ngOnInit() {
    if (isDevMode()) {
      console.log('👋 Development!');
    } else {
      console.log('💪 Production!');
    }

    this.getUserLogged();

  }

  verCategorias() {
    this.router.navigateByUrl('categories');
    this.menu.close();
  }

  products() {
    this.router.navigateByUrl('products');
    this.menu.close();
  }

  verPedidos() {
    this.router.navigateByUrl('pedidos');
    this.menu.close();
  }

  sideOrders() {
    this.router.navigateByUrl('side-orders');
    this.menu.close();
  }

  tipomenu() {
    this.router.navigateByUrl('tipos-menu');
    this.menu.close();
  }

  deals() {
    this.router.navigateByUrl('deals');
    this.menu.close();
  }


  getUserLogged() {
    // obtenemos el usuario para poder guardar la informacion de user/store
    this.userlogged = JSON.parse(localStorage.getItem('usuariostore'));
  }


}

