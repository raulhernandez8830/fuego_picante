import { ModalProductsComponent } from './../modal-products/modal-products.component';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-add-deal',
  templateUrl: './add-deal.component.html',
  styleUrls: ['./add-deal.component.scss'],
})
export class AddDealComponent implements OnInit {

  frm_deal: FormGroup;
  product: Product  = new Product();

  constructor(private modalctr: ModalController) {
    this.frm_deal = new FormGroup({
      'descripcion': new FormControl('', Validators.required),
      'descuento': new FormControl('', Validators.required),
      'inicio': new FormControl('', Validators.required),
      'fin': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {}

  saveDeal() {

  }

  async buscarProducto() {
    const modal = await this.modalctr.create({
      component: ModalProductsComponent,
      cssClass: ''
    });

    modal.onDidDismiss().then(
      (data) => {
        this.product = data['product'];
        console.log(this.product);
      }
    )

    return await modal.present();
  }

}
