import { ProductsPageModule } from './../products/products.module';
import { ModalProductsComponent } from './modal-products/modal-products.component';
import { ListProductsComponent } from './../products/list-products/list-products.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { DealDetailsComponent } from './deal-details/deal-details.component';
import { DealsListComponent } from './deals-list/deals-list.component';
import { AddDealComponent } from './add-deal/add-deal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DealsPageRoutingModule } from './deals-routing.module';

import { DealsPage } from './deals.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DealsPageRoutingModule,
    ComponentsModule,
    ReactiveFormsModule,
    ProductsPageModule
  ],
  declarations: [DealsPage, AddDealComponent, DealsListComponent, ModalProductsComponent, DealDetailsComponent]
})
export class DealsPageModule {}
