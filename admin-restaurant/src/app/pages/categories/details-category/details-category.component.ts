import { CategoriesService } from 'src/app/services/categories.service';
import { environment } from 'src/environments/environment';
import { ModalController, NavParams, LoadingController, ToastController } from '@ionic/angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { UserStore } from 'src/app/models/user_store';
import { Router } from '@angular/router';

@Component({
  selector: 'app-details-category',
  templateUrl: './details-category.component.html',
  styleUrls: ['./details-category.component.scss'],
})
export class DetailsCategoryComponent implements OnInit {

  frm_editcategoria: FormGroup;
  categoria: Category = new Category();
  url = environment.urlcategorias;
  imagen: File;

  constructor(private nav: NavParams, private router: Router,
    private toastctr: ToastController,
    private categoryservice: CategoriesService,
    private loadctr: LoadingController,
    private modalctr: ModalController) {
    this.frm_editcategoria = new FormGroup({
      'nombre': new FormControl('', Validators.required),
      'descripcion': new FormControl('', Validators.required),
      'imagen': new FormControl('')
    })
  }

  ngOnInit() {
    this.categoria = this.nav.get('categoria');
    this.frm_editcategoria.patchValue(this.categoria);
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }


  // guardar edicion
  onSubmit() {
    // obtenemos el usuario para poder guardar la informacion de user/store
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    console.log(userstore);

    // objeto categoria para llamar al service de guardado en la base de datos
    let categoria: Category = new Category;
    categoria = this.frm_editcategoria.value;
    categoria.id = this.categoria.id;
    categoria.store_id = userstore.store_id;
    categoria.user_id = userstore.id;

    this.frm_editcategoria.controls['imagen'].setValue(this.imagen);

    this.presentLoading();

    // llamada al metodo del service para editar una categoria
    this.categoryservice.updateCategory(categoria, this.imagen).subscribe(
      response => {},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.frm_editcategoria.reset();
        this.dismissModal();
      }
    )
  }

  dismissModal() {
    this.modalctr.dismiss();
  }

  onChangeInputFile(e){
    this.imagen = e.target.files[0];

  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Categoria editada con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al editar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

}
