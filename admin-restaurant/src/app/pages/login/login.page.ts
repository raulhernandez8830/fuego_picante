import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  frm_login: FormGroup;
  user: User;

  constructor(private userservice: UserService, private router: Router, private toastController: ToastController, private loadingController: LoadingController) {
    this.frm_login = new FormGroup({
      'email': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    })
  }

  ngOnInit() {
  }

  login() {
    this.presentLoading();
    // llamada la service
    this.userservice.validarCredenciales(this.frm_login.value).subscribe(
      response => {
        console.log(response);
        this.user = response;
        localStorage.setItem('usuariostore', JSON.stringify(response));
      },
      err => {},
      () => {
        if(Object.keys(this.user).length > 0) {
          this.router.navigateByUrl('dashboard');
          this.successToast();
        } else {
          this.errorToast();


        }
      }
    )
  }

  // toast para alerta de inicio de sesion
  async successToast() {
    const toast = await this.toastController.create({
      message: 'Credenciales validadas con exito!',
      duration: 2000,
      color:'success'
    });
    toast.present();
  }

  // toast para error en el inicio de sesion
  async errorToast() {
    const toast = await this.toastController.create({
      message: 'Error al validar credenciales',
      duration: 2000,
      color:'danger'
    });
    toast.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Validando Credenciales...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

}
