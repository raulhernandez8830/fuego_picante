import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SideOrdersPage } from './side-orders.page';

const routes: Routes = [
  {
    path: '',
    component: SideOrdersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SideOrdersPageRoutingModule {}
