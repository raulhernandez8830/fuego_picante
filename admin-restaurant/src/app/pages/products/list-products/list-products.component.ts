import { TipoMenu } from './../../../models/tipo_menu';

import { Category } from './../../../models/category';
import { CategoriesService } from './../../../services/categories.service';
import { ProductDetailComponent } from './../product-detail/product-detail.component';
import { ModalController, NavParams, LoadingController, ToastController } from '@ionic/angular';
import { Product } from './../../../models/product';
import { ProductsService } from './../../../services/products.service';
import { Component, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss'],
})
export class ListProductsComponent implements OnInit {

  @Input() estado: string;
  products: Product[] = [];
  categories: Category[] = [];
  urlproducts = environment.urlproducts;
  texto: string;
  tiposmenu: TipoMenu[] = [];
  frmnuevo = false;


  constructor(private productservice: ProductsService, private toastctr: ToastController,
    private loadctr: LoadingController,
    private categorieservice: CategoriesService,
    private modalctr: ModalController) { }

  ngOnInit() {

    this.getTiposMenu();
    this.getProducts();
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.categorieservice.getCategorias(userstore).subscribe(
      response => {
        this.categories = response;
      },
      err => {},
      () =>{}
    )
  }

  getProducts() {

    let userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.productservice.getProducts(userstore).subscribe(
      response => {
        this.products = response;
      },
      err => {},
      () => {}
    )
  }



  async editarProduct(product: Product) {
    const modal = await this.modalctr.create({
      component: ProductDetailComponent,
      componentProps: {
        'product': product,
        'categories': this.categories,
        'tiposmenu': this.tiposmenu
      },
      cssClass: 'modaledicionproducto'
    });

    modal.onDidDismiss().then(
      () => {
        this.getProducts();
      }
    )
    return await modal.present();
  }

  getTiposMenu() {
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));

    this.productservice.getTiposMenu(userstore).subscribe(
      response => {
        this.tiposmenu = response;
      },
      err => {},
      ()=> {}
    )
  }


  // seleccionar producto para deal
  seleccionarProductoByDeal(product: Product){
    this.modalctr.dismiss(product);
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Producto eliminado con exito.',
      duration: 3000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al eliminar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Eliminando información...',
      duration: 3000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  deleteProduct(product) {
    this.presentLoading();
    this.productservice.deleteProduct(product).subscribe(
      response  => {},
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.getProducts();
      }
    )
  }


}


