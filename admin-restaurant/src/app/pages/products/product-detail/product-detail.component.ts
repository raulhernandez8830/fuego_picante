import { TipoMenu } from './../../../models/tipo_menu';
import { ProductsService } from './../../../services/products.service';
import { Category } from 'src/app/models/category';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, ToastController, LoadingController, ModalController } from '@ionic/angular';
import { Product } from 'src/app/models/product';
import { CategoriesService } from 'src/app/services/categories.service';
import { environment } from 'src/environments/environment';
interface Usere {
  id: number;
  first: string;
  last: string;
}
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {

  categoria: number;
  frm_edit_product: FormGroup;
  prod: Product;
  categories: Category[] = [];
  imagenSeleccionada: File;
  tiposmenu: TipoMenu[] = [];
  urlproducts = environment.urlproducts;

  constructor(private router: Router, private modalctr: ModalController,
    private productservice: ProductsService,
    private loadctr: LoadingController,
    private toastctr: ToastController,
    private categoriaservice: CategoriesService,
    private navparam: NavParams) {
      this.frm_edit_product = new FormGroup({
      'image':            new FormControl(''),
      'nombre':           new FormControl('', Validators.required),
      'descripcion':      new FormControl('' ),
      'precio':           new FormControl('', Validators.required),
      'precio_oferta':    new FormControl(''),
      'categoria_id':     new FormControl('', Validators.required),
      'tipo_menu':        new FormControl('', Validators.required),
      'id':               new FormControl('', Validators.required),
      'estado':           new FormControl('', Validators.required),
      'dia':              new FormControl('')

      });



   }



    ngOnInit() {

      this.categories = this.navparam.get('categories');
      this.prod = this.navparam.get('product');
      this.frm_edit_product.patchValue(this.prod);
      console.clear();
      console.log(this.prod);
      this.tiposmenu = this.navparam.get('tiposmenu');

  }

  cancelaredicion() {
    this.modalctr.dismiss();
  }



  // guardamos edicion del producto
  guardarEdicion() {
    console.clear();
    console.log(this.imagenSeleccionada);
    this.frm_edit_product.controls['categoria_id'].setValue(
      (<HTMLInputElement>document.getElementById("categoriaprod")).value
    );

    this.frm_edit_product.controls['id'].setValue(this.prod.id);

    this.frm_edit_product.controls['tipo_menu'].setValue(
      (<HTMLInputElement>document.getElementById("tipomenu")).value
    );
    this.presentLoading();
    let userstore = JSON.parse(localStorage.getItem('usuariostore'));

    if(typeof this.imagenSeleccionada == 'undefined') {
      this.frm_edit_product.controls['image'].setValue('');
    } else {
      this.frm_edit_product.controls['image'].setValue(this.imagenSeleccionada);
    }


    console.log(this.frm_edit_product.value);

    //llamada al service para guardar la edicion
    this.productservice.editProduct(userstore,this.frm_edit_product.value).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.modalctr.dismiss();
        this.router.navigateByUrl('products');
      }
    )
  }



  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Producto editado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al guardar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'dots'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  onChangeInputFile(e){
    this.imagenSeleccionada = e.target.files[0];
  }
}


