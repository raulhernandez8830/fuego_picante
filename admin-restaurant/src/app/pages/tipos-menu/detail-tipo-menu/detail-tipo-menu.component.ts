import { NavParams, ModalController, ToastController, LoadingController } from '@ionic/angular';
import { ProductsService } from './../../../services/products.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { TipoMenu } from 'src/app/models/tipo_menu';

@Component({
  selector: 'app-detail-tipo-menu',
  templateUrl: './detail-tipo-menu.component.html',
  styleUrls: ['./detail-tipo-menu.component.scss'],
})
export class DetailTipoMenuComponent implements OnInit {

  frm_edittipomenu: FormGroup;
  tipomenu: TipoMenu = new TipoMenu();

  constructor(private productservice: ProductsService, private loadctr: LoadingController,
    private toastctr: ToastController,
    private modalctr: ModalController,
    private navparam: NavParams) {
    this.frm_edittipomenu = new FormGroup({
      'nombre': new FormControl(''),
      'descripcion': new FormControl('')
    });
  }

  ngOnInit() {
    this.tipomenu = this.navparam.get('tipomenu');
    this.frm_edittipomenu.patchValue(this.tipomenu);
  }

  onSubmit(){
    let tipomenu: TipoMenu = new TipoMenu();
    tipomenu = this.frm_edittipomenu.value;
    tipomenu.id = this.tipomenu.id;

    this.presentLoading();
    this.productservice.updateTipoMenu(this.frm_edittipomenu.value).subscribe(
      response => {

      },
      err => {
        this.errorToast();
      },
      () => {
        this.successToast();
        this.dismissModal();
      }
    )
  }

  async successToast() {
    const toast = await this.toastctr.create({
      message: 'Tipo de menu editado con exito.',
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  async errorToast() {
    const toast = await this.toastctr.create({
      message: 'Error al editar datos.',
      duration: 2000,
      color: 'danger'
    });
    toast.present();
  }

  // LOADER
  async presentLoading() {
    const loading = await this.loadctr.create({
      cssClass: 'my-custom-class',
      message: 'Guardando información...',
      duration: 2000,
      spinner: 'bubbles'
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

  }

  dismissModal() {
    this.modalctr.dismiss();
  }

}
