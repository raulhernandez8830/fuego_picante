import { DetailsPedidoComponent } from './../details-pedido/details-pedido.component';
import { ModalController } from '@ionic/angular';
import { PedidosService } from './../../../services/pedidos.service';
import { Component, OnInit } from '@angular/core';
import { UserStore } from 'src/app/models/user_store';

@Component({
  selector: 'app-list-pedidos',
  templateUrl: './list-pedidos.component.html',
  styleUrls: ['./list-pedidos.component.scss'],
})
export class ListPedidosComponent implements OnInit {

  pedidos: any[];
  texto: string;

  constructor(private pedidoservice: PedidosService, private modalctr: ModalController ) { }

  ngOnInit() {
    this.getPedidos();
  }

  getPedidos() {
    let userstore: UserStore = new UserStore();
    userstore = JSON.parse(localStorage.getItem('usuariostore'));
    this.pedidoservice.getPedidos(userstore.store_id).subscribe(
      response => {
        this.pedidos = response;
      }
    )
  }

  async detalles(p) {
    const modal = await this.modalctr.create({
      component: DetailsPedidoComponent,
      cssClass: 'modaledicionproducto',
      componentProps: {
        pedido: p
      }
    });
    return await modal.present();
  }



}
