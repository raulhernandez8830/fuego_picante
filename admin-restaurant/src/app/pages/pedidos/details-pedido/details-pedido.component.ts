import { ModalController, NavParams } from '@ionic/angular';
import { PedidosService } from './../../../services/pedidos.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-details-pedido',
  templateUrl: './details-pedido.component.html',
  styleUrls: ['./details-pedido.component.scss'],
})
export class DetailsPedidoComponent implements OnInit {

  pedido: any;
  detalles: any[] = [];
  urlproducts = environment.urlproducts;
  nombrecliente: string;

  constructor(private pedidoservice: PedidosService, private modalctr: ModalController, private navparam: NavParams) { }

  ngOnInit() {
    this.pedido = this.navparam.get('pedido');
    this.getDetailPedido();
    this.nombrecliente = this.pedido.nombre_cliente + ' ' + this.pedido.apellido_cliente;
  }

  getDetailPedido() {
    this.pedidoservice.getDetailPedido(this.pedido).subscribe(
      response => {
        this.detalles = response;
      }
    );
  }

  dismissModal() {
    this.modalctr.dismiss();
  }

}
