import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { DetailsPedidoComponent } from './details-pedido/details-pedido.component';
import { ListPedidosComponent } from './list-pedidos/list-pedidos.component';
import { AddPedidoComponent } from './add-pedido/add-pedido.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PedidosPageRoutingModule } from './pedidos-routing.module';

import { PedidosPage } from './pedidos.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PedidosPageRoutingModule,
    ComponentsModule,
    PipesModule
  ],
  declarations: [PedidosPage, AddPedidoComponent, ListPedidosComponent, DetailsPedidoComponent]
})
export class PedidosPageModule {}
