import { ListProductsComponent } from './../../products/list-products/list-products.component';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-pedido',
  templateUrl: './add-pedido.component.html',
  styleUrls: ['./add-pedido.component.scss'],
})
export class AddPedidoComponent implements OnInit {

  constructor(private modalctr: ModalController) { }

  ngOnInit() {}



  async verProductos() {
    const modal = await this.modalctr.create({
      component: ListProductsComponent,
      cssClass: '',
      componentProps: {
        estado: 'true'
      }
    });
    return await modal.present();
  }

}
